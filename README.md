# Steam

[![dev](https://img.shields.io/badge/docs-latest-blue?style=for-the-badge&logo=julia)](https://ExpandingMan.gitlab.io/Steam.jl/)

A Julia wrapper of the [Steam Web API](https://developer.valvesoftware.com/wiki/Steam_Web_API).

Using this requires a Steam API key, which can be generated
[here](https://steamcommunity.com/dev/apikey).


This package is unofficial and not affiliated with Valve in any way.

