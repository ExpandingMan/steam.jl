module Steam

using HTTP, URIs, JSON3

const DEFAULT_STEAM_HOST = "https://api.steampowered.com"


struct SteamAPI
    host::URI
    key::String
end

function SteamAPI(key::AbstractString;
                  host::Union{AbstractString,URI}=DEFAULT_STEAM_HOST,
                 )
    SteamAPI(URI(host), key)
end

function SteamAPI(;kw...)
    key = get(ENV, "STEAM_API_KEY", nothing)
    if isnothing(key) || isempty(key)
        throw(ArgumentError("no steam API key was provided"))
    end
    SteamAPI(key; kw...)
end

function makeurl(api::SteamAPI, path::AbstractString...; kw...)
    url = joinpath(api.host, path...)
    query = Dict{String,Any}(string(k)=>v for (k,v) ∈ kw if v ≠ false)
    query["key"] = api.key
    URI(url; query)
end


function raw_getappnews(api::SteamAPI, appid::Integer;
                        count::Integer=1,
                        maxlength::Integer=512,
                        format::AbstractString="json",
                       )
    url = makeurl(api, "ISteamNews", "GetNewsForApp", "v0002/";
                  appid, count, maxlength, format)
    HTTP.get(url)
end

function raw_getglobalachievements(api::SteamAPI, appid::Integer;
                                   format::AbstractString="json",
                                  )
    url = makeurl(api, "ISteamUserStats", "GetGlobalAchievementPercentagesForApp", "v0002";
                  gameid=appid, format)
    HTTP.get(url)
end

function raw_getplayersummaries(api::SteamAPI, userids;
                                format::AbstractString="json",
                               )
    ids = userids isa Union{AbstractString,Integer} ? userids : join(userids, ",")
    url = makeurl(api, "ISteamUser", "GetPlayerSummaries", "v0002/";
                  steamids=userids, format)
    HTTP.get(url)
end

function raw_getfriends(api::SteamAPI, id::Integer;
                        relationship::AbstractString="friend",
                        format::AbstractString="json",
                       )
    url = makeurl(api, "ISteamUser", "GetFriendList", "v0001/";
                  steamid=id, relationship, format)
    HTTP.get(url)
end

function raw_getachievements(api::SteamAPI, id::Integer, appid::Integer)
    url = makeurl(api, "ISteamUserStats", "GetPlayerAchievements", "v0001/";
                  steamid=id, appid)
    HTTP.get(url)
end

function raw_getuserstats(api::SteamAPI, id::Integer, appid::Integer)
    url = makeurl(api, "ISteamUserStats", "GetUserStatsForGame", "v0002/";
                  steamid=id, appid)
    HTTP.get(url)
end

function raw_getownedgames(api::SteamAPI, id::Integer;
                           include_appinfo::Bool=false,
                           include_played_free_games::Bool=false,
                           format::AbstractString="json",
                          )
    url = makeurl(api, "IPlayerService", "GetOwnedGames", "v0001/";
                  steamid=id, include_appinfo, include_played_free_games,
                  format)
    HTTP.get(url)
end

function raw_getrecentlyplayed(api::SteamAPI, id::Integer;
                               count::Integer=8,
                               format::AbstractString="json",
                              )
    url = makeurl(api, "IPlayerService", "GetRecentlyPlayedGames", "v0001/";
                  steamid=id, count, format)
    HTTP.get(url)
end


getappnews(api::SteamAPI, appid::Integer; kw...) = JSON3.read(raw_getappnews(api, appid; kw...).body)[:appnews]
function getglobalachievements(api::SteamAPI, appid::Integer)
    JSON3.read(raw_getglobalachievements(api, appid).body)[:achievementpercentages]
end
function getplayersummaries(api::SteamAPI, userids)
    JSON3.read(raw_getplayersummaries(api, userids).body)[:response][:players]
end
function getfriends(api::SteamAPI, id::Integer; kw...)
    JSON3.read(raw_getfriends(api, id; kw...).body)[:friendslist][:friends]
end
function getachievements(api::SteamAPI, id::Integer, appid::Integer)
    JSON3.read(raw_getachievements(api, id, appid).body)[:playerstats]
end
function getuserstats(api::SteamAPI, id::Integer, appid::Integer)
    JSON3.read(raw_getuserstats(api, id, appid).body)[:playerstats]
end
function getownedgames(api::SteamAPI, id::Integer; kw...)
    JSON3.read(raw_getownedgames(api, id; kw...).body)[:response][:games]
end
function getrecentlyplayed(api::SteamAPI, id::Integer; kw...)
    JSON3.read(raw_getrecentlyplayed(api, id; kw...).body)[:response][:games]
end


export SteamAPI


end
