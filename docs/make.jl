using Steam
using Documenter

DocMeta.setdocmeta!(Steam, :DocTestSetup, :(using Steam); recursive=true)

makedocs(;
    modules=[Steam],
    authors="Expanding Man <savastio@protonmail.com> and contributors",
    repo="https://gitlab.com/ExpandingMan/Steam.jl/blob/{commit}{path}#{line}",
    sitename="Steam.jl",
    format=Documenter.HTML(;
        prettyurls=get(ENV, "CI", "false") == "true",
        canonical="https://ExpandingMan.gitlab.io/Steam.jl",
        assets=String[],
    ),
    pages=[
        "Home" => "index.md",
    ],
)
